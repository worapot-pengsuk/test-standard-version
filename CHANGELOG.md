# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.1](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.3.0%0Dversion/1.3.1) (2020-07-03)

## [1.3.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.3.0-alpha.1%0Dversion/1.3.0) (2020-07-03)

## [1.3.0-alpha.1](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.3.0-alpha.0%0Dversion/1.3.0-alpha.1) (2020-07-03)

## [1.3.0-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.2%0Dversion/1.3.0-alpha.0) (2020-06-26)

## [1.2.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.2.0-alpha.0%0Dversion/1.2.0) (2020-06-26)


### Others

* **release:** 1.2.0 [skip ci] ([e3aca52](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/e3aca527cc67a217617e686ded69daaac5fbde0d))

## [1.2.0-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.1%0Dversion/1.2.0-alpha.0) (2020-06-26)


### Features

* **Index:** add feature 6 ([01d49de](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/01d49de1e0420b97bbeff4e120c5c52f59a0ef82))


### Others

* **release:** 1.2.0-alpha.0 [skip ci] ([f88c597](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/f88c5974603b02d8f9527fed52fbc0533c0cdcdb))

## [1.2.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.2.0-alpha.0%0Dversion/1.2.0) (2020-06-26)

## [1.2.0-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.1%0Dversion/1.2.0-alpha.0) (2020-06-26)


### Features

* **Index:** add feature 6 ([01d49de](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/01d49de1e0420b97bbeff4e120c5c52f59a0ef82))
### [1.1.2](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.2-alpha.0%0Dversion/1.1.2) (2020-06-26)

### [1.1.2-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.1%0Dversion/1.1.2-alpha.0) (2020-06-26)


### Bug Fixes

* change 1 to 0 ([6226bec](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/6226bec306e7f85b26b92f09fa56738449818be4))


### CI

* update pipeline ([fec4496](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/fec4496a4c327d564ba0d9deb9e68ee0de2d6765))

### [1.1.1](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.1-alpha.0%0Dversion/1.1.1) (2020-06-26)

### [1.1.1-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.0%0Dversion/1.1.1-alpha.0) (2020-06-26)


### Bug Fixes

* remove welcome message ([a7e1d17](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/a7e1d17c437d20cb5016f227504da4eae6cdf5a2))

## [1.1.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.0-alpha.2%0Dversion/1.1.0) (2020-06-26)

## [1.1.0-alpha.2](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.0-alpha.1%0Dversion/1.1.0-alpha.2) (2020-06-26)


### Bug Fixes

* **Index:** add missing feature 3 ([0d823c4](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/0d823c4907ef879c10dc8e8db6a8381864c5b50a))

## [1.1.0-alpha.1](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.1.0-alpha.0%0Dversion/1.1.0-alpha.1) (2020-06-26)


### Features

* **Index:** add feature 5 ([b10e79c](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/b10e79cda84de2b5fcb985183a1b9e72050e9416))

## [1.1.0-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/1.0.1-alpha.0%0Dversion/1.1.0-alpha.0) (2020-06-26)


### Features

* **Index:** add feature 4 ([d73938a](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/d73938af75aa8c00ac2506a1fc48adabbbab5b0d))

### [1.0.1-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/0.1.3-alpha.1%0Dversion/1.0.1-alpha.0) (2020-06-26)


### Others

* inc version to 1.0.0 [skip ci] ([559e6fc](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/559e6fc4843c5e918c88e9d833fe2bef75b844a3))

### [0.1.3-alpha.1](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/0.1.3-alpha.0%0Dversion/0.1.3-alpha.1) (2020-06-26)


### Bug Fixes

* **Git:** not ignore package.json ([24d10bc](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/24d10bc3014e4e9d4ab46e2e9ba441483964a16c))

### [0.1.3-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/0.1.2%0Dversion/0.1.3-alpha.0) (2020-06-26)


### Bug Fixes

* **Index:** remove welcome message ([346d0ab](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/346d0ab5c572cedd268dca88ed07ed3722f3bfde))

### [0.1.2](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/0.1.2-alpha.1%0Dversion/0.1.2) (2020-06-26)

### [0.1.2-alpha.1](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/0.1.2-alpha.0%0Dversion/0.1.2-alpha.1) (2020-06-26)


### Features

* **Index:** add feat 2 ([7b02d14](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/7b02d1426fb0d4654264404cf6822b5cc0703e18))

### [0.1.2-alpha.0](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/0.1.1%0Dversion/0.1.2-alpha.0) (2020-06-26)


### Features

* **Index:** add feature 1 ([906c0f1](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/906c0f11157822287691eff4070cc479a15fb816))

### [0.1.1](https://bitbucket.org/worapot-pengsuk/test-standard-version/compare/version/0.1.1-alpha.1%0Dversion/0.1.1) (2020-06-26)

### 0.1.1-alpha.1 (2020-06-26)


### Features

* **Index:** remove content on index page ([e931d72](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/e931d720db66e1978ab6c4068a08365940d875dd))
* **Version:** add `standard-version` pipeline on `master` branch ([eae41f1](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/eae41f101dd946f76f96a524f701f000b6c33304))


### Bug Fixes

* **Pipeline:** fail on master ([087c04d](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/087c04daac29c689cdb3ae58d4b05c40bc545627))


### Others

* **release:** 0.1.0 [skip ci] ([7c68f88](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/7c68f883be825da7d54a34a2f3466af265400b89))
* **release:** 0.1.1-alpha.0 [skip ci] ([4157878](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/415787850e058cfa8ef084f65e7773d8a99ba8ba))

### 0.1.1-alpha.0 (2020-06-26)


### Features

* **Version:** add `standard-version` pipeline on `master` branch ([eae41f1](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/eae41f101dd946f76f96a524f701f000b6c33304))


### Bug Fixes

* **Pipeline:** fail on master ([087c04d](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/087c04daac29c689cdb3ae58d4b05c40bc545627))


### Others

* **release:** 0.1.0 [skip ci] ([7c68f88](https://bitbucket.org/worapot-pengsuk/test-standard-version/commits/7c68f883be825da7d54a34a2f3466af265400b89))

## 0.1.0 (2020-06-26)


### Features

* **Version:** add `standard-version` pipeline on `master` branch eae41f1
